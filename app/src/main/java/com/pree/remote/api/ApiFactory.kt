package com.pree.remote.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object ApiFactory {

    //Creating Auth Interceptor to add api_key query in front of all the requests.

    private var ApiBaseUrl: String = "https://therci.in/api/"
    private val authInterceptor = Interceptor {chain->
        val newUrl = chain.request().url()
            .newBuilder()
            .build()

        val newRequest = chain.request()
            .newBuilder()
            .url(newUrl)
            .build()
        chain.proceed(newRequest)
    }

    private var httpLoggingInterceptor = HttpLoggingInterceptor()
        .setLevel(HttpLoggingInterceptor.Level.BODY)

    private val httpCLient = OkHttpClient().newBuilder()
        .addInterceptor(authInterceptor)
        .addInterceptor(httpLoggingInterceptor)
        .addInterceptor(CustomInterceptor())
        .addNetworkInterceptor(CustomInterceptor())
        .build()

    fun retrofit() : Retrofit = Retrofit.Builder()
        .client(httpCLient)
        .baseUrl(ApiBaseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

    val kuuezaApi : KuuezaApi = retrofit().create(KuuezaApi::class.java)
}