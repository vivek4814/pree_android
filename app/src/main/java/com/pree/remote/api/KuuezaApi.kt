package com.pree.remote.api

import com.pree.model.request.LoginRequest
import com.pree.model.response.LoginResponse
import retrofit2.Call
import retrofit2.http.*

interface KuuezaApi {
    @POST("auth/login")
    fun getLoginApi(@Body loginRequest: LoginRequest): Call<LoginResponse>
//
//    @FormUrlEncoded
//    @POST("auth/register")
//    fun registerUser(@FieldMap id: Map<String, String>): Call<SignupResponse>
//
//    @FormUrlEncoded
//    @POST("auth/verifyotp")
//    fun verifyOtp(@Header("Authorization") auth: String, @Field("otp") id: String): Call<SignupResponse>
}