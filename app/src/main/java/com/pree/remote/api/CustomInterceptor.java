package com.pree.remote.api;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class CustomInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        /*
        chain.request() returns original request that you can work with(modify, rewrite)
        */
        Request request = chain.request();
        // Here you can rewrite the request
        /*
        chain.proceed(request) is the call which will initiate the HTTP work. This call invokes the
        request and returns the response as per the request.
        */
        Response response = chain.proceed(request);

        if(response.message().equals("Unauthorized")){
            // Logout application due to security reasion
            Log.d("Unauthorized",response.message());

        }

        Log.d("CustomInterceptor",response.toString());
        //Here you can rewrite/modify the response
        return response;
    }
}
