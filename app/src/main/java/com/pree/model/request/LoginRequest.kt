package com.pree.model.request

class LoginRequest {
    var username: String =""
    var password: String = ""
}