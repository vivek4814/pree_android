package com.pree.model.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class LoginResponse {
    @SerializedName("access_token")
    @Expose
    private var accessToken: String? = null

    @SerializedName("token_type")
    @Expose
    private var tokenType: String? = null

    @SerializedName("expires_at")
    @Expose
    private var expiresAt: String? = null

    fun getAccessToken(): String? {
        return accessToken
    }

    fun setAccessToken(accessToken: String?) {
        this.accessToken = accessToken
    }

    fun getTokenType(): String? {
        return tokenType
    }

    fun setTokenType(tokenType: String?) {
        this.tokenType = tokenType
    }

    fun getExpiresAt(): String? {
        return expiresAt
    }

    fun setExpiresAt(expiresAt: String?) {
        this.expiresAt = expiresAt
    }
}