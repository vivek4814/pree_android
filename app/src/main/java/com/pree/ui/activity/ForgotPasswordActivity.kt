package com.pree.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.pree.R
import com.pree.ui.activity.baseActivity.BaseActivity
import com.pree.utils.SlideAnimationUtil
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPasswordActivity : BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        initialiseUI()
        setupClickListener()
    }

    private fun initialiseUI()
    {
    }

    private fun setupClickListener()
    {
        iv_toolbarBack.setOnClickListener(this)
        btn_send.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.iv_toolbarBack ->
            {
                onBackPressed()
            }
            R.id.btn_send ->
            {
//                val signUpIntent = Intent(this@ForgotPasswordActivity, ForgotPasswordActivity::class.java)
//                startActivity(signUpIntent)
//                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        }
    }
}