package com.pree.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import com.pree.R
import com.pree.ui.activity.baseActivity.BaseActivity
import com.pree.ui.adapter.MakeSpinnerListAdapter
import com.pree.utils.SlideAnimationUtil
import kotlinx.android.synthetic.main.activity_add_new_vehicles.*
import kotlinx.android.synthetic.main.activity_vehicles_list.*
import kotlinx.android.synthetic.main.activity_vehicles_list.iv_back

class AddNewVehiclesActivity: BaseActivity(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_vehicles)
        setupSpinner()
        setupClickListener()
    }

    private fun setupSpinner()
    {
        //For make List
        val makeList = ArrayList<String>()
        makeList.add("Select")
        makeList.add("A")
        makeList.add("B")
        makeList.add("C")
        val makeSelectionAdapter = MakeSpinnerListAdapter(this@AddNewVehiclesActivity, makeList)
        spinner_make.adapter = makeSelectionAdapter
        spinner_make.onItemSelectedListener = this

        val modelList = ArrayList<String>()
        modelList.add("Select")
        modelList.add("A")
        modelList.add("B")
        modelList.add("C")
        val modelSelectionAdapter = MakeSpinnerListAdapter(this@AddNewVehiclesActivity, modelList)
        spinner_model.adapter = modelSelectionAdapter
        spinner_model.onItemSelectedListener = this

        val yearList = ArrayList<String>()
        yearList.add("Select")
        yearList.add("2018")
        yearList.add("2019")
        yearList.add("2020")
        val yearSelectionAdapter = MakeSpinnerListAdapter(this@AddNewVehiclesActivity, yearList)
        spinner_year.adapter = yearSelectionAdapter
        spinner_year.onItemSelectedListener = this

        val colorList = ArrayList<String>()
        colorList.add("Select")
        colorList.add("White")
        colorList.add("Black")
        colorList.add("Red")
        val colorSelectionAdapter = MakeSpinnerListAdapter(this@AddNewVehiclesActivity, colorList)
        spinner_color.adapter = colorSelectionAdapter
        spinner_color.onItemSelectedListener = this
    }

    private fun setupClickListener()
    {
        iv_back.setOnClickListener(this)
    }
    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.iv_back ->
            {
                onBackPressed()
            }
//            R.id.iv_addVehicle ->
//            {
//                val signUpIntent = Intent(this@AddNewVehiclesActivity, ForgotPasswordActivity::class.java)
//                startActivity(signUpIntent)
//                SlideAnimationUtil.slideNextAnimation(this@AddNewVehiclesActivity)
//            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
    }
}