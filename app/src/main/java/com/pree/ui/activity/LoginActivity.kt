package com.pree.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.pree.R
import com.pree.model.request.LoginRequest
import com.pree.model.response.LoginResponse
import com.pree.remote.api.ApiFactory
import com.pree.ui.activity.baseActivity.BaseActivity
import com.pree.utils.SlideAnimationUtil
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setupClickListener()
    }

    private fun setupClickListener()
    {
        btn_login.setOnClickListener(this)
        tv_forgotPasswordLabel.setOnClickListener(this)
        tv_newAccountLabel.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.btn_login ->
            {
                //initiateLoginProcess()
                val intent = Intent(this@LoginActivity, MapActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                finish()
                SlideAnimationUtil.slideNextAnimation(this@LoginActivity)
            }
            R.id.tv_forgotPasswordLabel ->
            {
                val signUpIntent = Intent(this@LoginActivity, ForgotPasswordActivity::class.java)
                startActivity(signUpIntent)
                SlideAnimationUtil.slideNextAnimation(this@LoginActivity)
            }
            R.id.tv_newAccountLabel ->
            {
                val signUpIntent = Intent(this@LoginActivity, CreateAccountActivity::class.java)
                startActivity(signUpIntent)
                SlideAnimationUtil.slideNextAnimation(this@LoginActivity)
            }
        }
    }

    private fun initiateLoginProcess()
    {
        val loginRequest: LoginRequest = LoginRequest()
        loginRequest.username = "tests"
        loginRequest.password = "123456789"
        val call: Call<LoginResponse> = ApiFactory.kuuezaApi.getLoginApi(loginRequest)
        call.enqueue(object : Callback<LoginResponse>
        {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                if (response.isSuccessful)
                {
                    Toast.makeText(this@LoginActivity, "Login successfully", Toast.LENGTH_SHORT).show()
                }
                else
                {
                    Toast.makeText(this@LoginActivity, "Some error occur during your request", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                Toast.makeText(this@LoginActivity, "Something went wrong, please check and try again", Toast.LENGTH_SHORT).show()
            }
        })
    }
}