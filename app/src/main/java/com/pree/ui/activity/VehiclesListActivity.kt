package com.pree.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.pree.R
import com.pree.ui.activity.baseActivity.BaseActivity
import com.pree.ui.adapter.VehicleListAdapter
import com.pree.utils.RecyclerViewItemDecorator
import com.pree.utils.SlideAnimationUtil
import kotlinx.android.synthetic.main.activity_vehicles_list.*

class VehiclesListActivity: BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vehicles_list)
        setupVehiclesList()
        setupClickListener()
    }

    private fun setupVehiclesList()
    {
        val mVehiclesList = ArrayList<String>()
        val mVehicleListAdapter = VehicleListAdapter(this@VehiclesListActivity, mVehiclesList)
        val mLayoutManager = LinearLayoutManager(this@VehiclesListActivity)
        rv_vehiclesList?.layoutManager = mLayoutManager
        rv_vehiclesList?.adapter = mVehicleListAdapter
        rv_vehiclesList?.hasFixedSize()
        rv_vehiclesList?.addItemDecoration(RecyclerViewItemDecorator(this@VehiclesListActivity))
        rv_vehiclesList?.isNestedScrollingEnabled
    }

    private fun setupClickListener()
    {
        iv_back.setOnClickListener(this)
        iv_addVehicle.setOnClickListener(this)
    }
    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.iv_back ->
            {
                onBackPressed()
            }
            R.id.iv_addVehicle ->
            {
                val signUpIntent = Intent(this@VehiclesListActivity, AddNewVehiclesActivity::class.java)
                startActivity(signUpIntent)
                SlideAnimationUtil.slideNextAnimation(this@VehiclesListActivity)
            }
        }
    }
}