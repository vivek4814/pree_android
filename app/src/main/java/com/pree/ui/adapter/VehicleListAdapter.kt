package com.pree.ui.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pree.R
import java.text.SimpleDateFormat
import java.util.*


class VehicleListAdapter (context: Context, vehicleList: ArrayList<String>): RecyclerView.Adapter<VehicleListAdapter.MyViewHolder>() {

    private var mContext: Context? = null
    private var mVehicleList: ArrayList<String>? = null
    init {
        this.mContext = context
        this.mVehicleList = vehicleList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.item_vehicle_list, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 10//mEventList?.size!!
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
//        val eventItem = mEventList?.get(position)
//
//        val curFormat = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss")
//        val eventDate: Date = curFormat.parse(eventItem?.start)
//        holder.mEventTime?.text =  DateUtils.getTimeFromDate(eventDate, "hh:mm a") //eventItem?.date_hour
//        holder.mEventTitle?.text = eventItem?.title
//        //val color = Color.parseColor("#00FFFF")
//        (holder.mCircleView?.getBackground() as GradientDrawable).setColor(Color.parseColor(eventItem?.color))
//        //holder.mCircleView?.setBackgroundColor(Color.parseColor(eventItem?.color))
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

//        var mCircleView: ImageView? = null
//        var mEventTime: TextView? = null
//        var mEventTitle: TextView? = null
//
//        init {
//            mCircleView = itemView.findViewById(R.id.tv_circle)
//            mEventTime = itemView.findViewById(R.id.tv_eventTime)
//            mEventTitle = itemView.findViewById(R.id.tv_eventTitle)
//        }
    }
}